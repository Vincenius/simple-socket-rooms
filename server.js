const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
 
app.use(express.static(__dirname + '/static'));
app.set('view engine', 'ejs');

var roomData = {}

app.get('/', function (req, res) {
    let randomRoom = Math.random().toString(36).substring(2, 6);
    
    // generate random room id until unused id found
    while (randomRoom in roomData) {
        randomRoom = Math.random().toString(36).substring(2, 6);
    }

    res.redirect(randomRoom);
});

app.get('/:roomId', function (req, res) {
    // var roomId = req.params.roomId;
    res.render('index');
});

// user connects
io.on('connection', function(socket){
    // referer is full uri - we only want the last part
    const roomId = socket.request.headers.referer.split('/').pop();

    socket.join(roomId);
    
    if (!(roomId in roomData)) {
        // if room does not exist create new room with empty player object
        roomData[roomId] = { };
        roomData[roomId].player = { };
    } else {
        // if room exists provide roomData to new connected user
        io.to(socket.id).emit('updateRoom', roomData[roomId]);
    }

    // user joins room
    socket.on('joinRoom', function(username) {
        // add player to room
        roomData[roomId].player[socket.id] = { 
            username // short for 'username: username' -> add additional user data here
        }; 

        io.to(roomId).emit('updateRoom', roomData[roomId]);
    });

    // user leaves room
    socket.on('disconnect', function() {
        delete roomData[roomId].player[socket.id];
        io.to(roomId).emit('updateRoom', roomData[roomId]);
     });
});
 
//_____Start server_____/
http.listen(8080, function () {
    console.log('listening on *:8080');
});